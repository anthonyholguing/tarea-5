const consultarData = (name, apell) => {
  const xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      let respuesta = JSON.parse(xhttp.responseText);
      let DatosEstudiante = respuesta.DatosEstudiante;
      DatosEstudiante.map((estudiante) => {
        if (name === estudiante.Nombre && apell === estudiante.Apellido) {
          recibirDato(estudiante);
        }
      });
    }
  };
  xhttp.open("GET", "archivo.json", true);
  xhttp.send();
};
const recibirDato = (estudiante) => {
  const { Nombre, Semestre, Paralelo, Dirrecion,  Telefono, Cedula ,Correo } = estudiante;
  document.getElementById("datos").innerHTML = `
    <div>
        <p> <strong> Nombre:    </strong> ${Nombre}    </p> 
        <p> <strong> Semestre:  </strong> ${Semestre}  </p> 
        <p> <strong >Paralelo:  </strong> ${Paralelo}  </p> 
        <p> <strong> Dirección: </strong> ${Dirrecion} </p> 
        <p> <strong> Teléfono:  </strong> ${Telefono}  </p> 
        <p> <strong> Cédula:    </strong> ${Cedula}    </p> 
        <p> <strong> Correo:    </strong> ${Correo}    </p> 
    </div>
    `;
};
const mostrarError = () => {
  document.getElementById("datos").innerHTML = "<p>404 Not Found</p>";
};
